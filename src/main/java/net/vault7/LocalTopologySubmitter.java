package net.vault7;

import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.starter.util.StormRunner;

class LocalTopologySubmitter extends TopologySubmitter {


  private static final int DEFAULT_RUNTIME_IN_SECONDS = 30;

  private LocalTopologySubmitter(String topologyName) {
    super(topologyName);
  }


  public static void main(String[] args)
      throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {
    new LocalTopologySubmitter(RemoteTopologySubmitter.class.getSimpleName() + "-topology").run();
  }


  void run()
      throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {
    StormRunner.runTopologyLocally(builder.createTopology(), topologyName, topologyConfig,
        DEFAULT_RUNTIME_IN_SECONDS);
  }
}
