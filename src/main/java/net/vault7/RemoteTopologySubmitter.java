package net.vault7;

import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;

class RemoteTopologySubmitter extends TopologySubmitter {

  private RemoteTopologySubmitter(String topologyName) {
    super(topologyName);
  }


  public static void main(String[] args)
      throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {
    new RemoteTopologySubmitter(RemoteTopologySubmitter.class.getSimpleName() + "-topology").run();
  }


  void run()
      throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {
    StormSubmitter.submitTopology(topologyName, topologyConfig, builder.createTopology());
  }

}
