package net.vault7;

import org.apache.storm.Config;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;

abstract class TopologySubmitter {

  private static final String SPOUT_ID = "micro-batching-spout";
  private static final String BOLT_ID = "micro-batching-bolt";

  final TopologyBuilder builder;
  final String topologyName;
  final Config topologyConfig;


  TopologySubmitter(String topologyName) {
    this.topologyName = topologyName;
    builder = new TopologyBuilder();
    topologyConfig = createTopologyConfig();
    wireTopology();
  }


  private static Config createTopologyConfig() {
    Config config = new Config();
    config.setDebug(false);
    config.setMessageTimeoutSecs(3);
    return config;
  }

  abstract void run()
      throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException;


  private void wireTopology() {
    builder.setSpout(SPOUT_ID, new AddressSpout(), 1);
    builder.setBolt(BOLT_ID, new TickTupleBatchBolt(), 1).shuffleGrouping(SPOUT_ID);
  }

}
